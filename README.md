# ubersoft:jqtree
Wrapper for jqTree. JqTree is a jQuery widget for displaying a tree structure in html. It supports json data, loading via ajax and drag-and-drop.
# jqTree
[jqTree](https://mbraak.github.io/jqTree/).
