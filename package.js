Package.describe({
  name: 'ubersoft:jqtree',
  version: '1.3.7',
  summary: 'Wrapper for jqtree.js',
  git: 'https://bitbucket.org/ubersoftinc/jqtree.git',
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.4.3.2');
  api.use(['meteor', 'ddp', 'jquery']);
  api.addFiles(['client/tree.jquery.js', 'client/jqtree.css'], ['client']);
});
